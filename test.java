import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.DoubleBinaryOperator;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

class Main {

	public static void main (String[] args) {

		System.out.println("Hello");
		//25/09 je viens de corriger une ligne

		System.out.println("Nouvelle ligne");

		System.out.println("Ajout de deux nombres");
		int a = 10;
		int b = 20;
		int result = a+b;

	
		//j'ajoute sur le d�pot distant origin ce commentaire

		//Exercice 1 : Déclarer une liste de String, en extraire un stream et afficher
		List<String> listString = new ArrayList<String>();
		listString.add("c");
		listString.add("h");
		listString.add("r");
		listString.add("i");
		listString.add("s");
		listString.add("t");

		Stream<String> listStream = listString.stream();
		listStream.forEach(System.out::print);
		System.out.println();

		//Exercice 2 : Concaténer le stream d une liste avec le stream d un tableau de String

		Stream<String> listStream1 = listString.stream();


		String[] tabString = new String[4];
		tabString[0]="o";
		tabString[1]="p";
		tabString[2]="h";
		tabString[3]="e";

		Stream<String> listStream2 = Arrays.stream(tabString);

		Stream.concat(listStream1,listStream2).forEach(System.out::print);

		System.out.println();

		// Exercice 3 : Créer un Stream de double générés aléatoirement par la méthode Math.random(),
		// puis les afficher.

		Stream<Double> doubleStream = Stream.generate(Math::random).limit(10);

		doubleStream.forEach(System.out::print);

		
	}

}
